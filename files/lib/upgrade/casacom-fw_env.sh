casacom_save_fw_env()
{
	local filelist="$1"

	# save fw-env
	fw_printenv > /etc/fw_env
	echo /etc/fw_env >> $filelist

	fw_printenv > /tmp/fw_env
	cp /usr/sbin/fw_setenv /tmp/fw_setenv
	cp /bin/busybox /tmp/busybox
	cp /etc/fw_env.config /tmp/fw_env.config
}

sysupgrade_init_conffiles="$sysupgrade_init_conffiles casacom_save_fw_env"

