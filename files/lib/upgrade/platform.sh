platform_check_image() {
        # i know no way to verify the image
        return 0;
}

platform_do_upgrade() {
        sync
        get_image "$1" | dd of=/dev/mmcblk0 bs=2M conv=fsync
        sleep 1
}

platform_copy_config() {
	v "Restoring fw env ..."
	/tmp/busybox ln -s /tmp /var
	cp /tmp/fw_env.config /etc/fw_env.config
	cat /tmp/fw_env | while read LINE
	do
		param=$( echo "$LINE" | cut -d= -f1 )
		value=$( echo "$LINE" | cut -d= -f2 )
		/tmp/fw_setenv $param "$value"
	done

	v "Copying configuration to first partition"
	mount -t vfat -o rw,noatime /dev/mmcblk0p1 /mnt
	cp -af "$CONF_TAR" /mnt/
	sync
	umount /mnt
}
