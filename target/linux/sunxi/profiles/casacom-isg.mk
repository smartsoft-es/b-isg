#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/Casacom_ISG
	NAME:=Casacom ISG
	PACKAGES:=\
		uboot-sunxi-Casacom_ISG kmod-rtc-sunxi swconfig
endef

define Profile/Casacom_ISG/Description
	Package set optimized for the Casacom ISG
endef

$(eval $(call Profile,Casacom_ISG))
